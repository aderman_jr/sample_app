require 'test_helper'

class UsersLinksTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end
  
  
  test "should get links after login" do
    log_in_as(@user, remember_me: '0')
    get users_path
    assert_equal users_path, '/users'
    assert_select "a[href=?]", users_path, count: 1
    assert_select "a[href=?]", edit_user_path(@user), count: 1
    assert_select "a[href=?]", user_path(@user), count: 2
    
  end
  
  test "should not get links without login" do
    get login_path
    assert_select "a[href=?]", users_path, count: 0
    assert_select "a[href=?]", edit_user_path(@user), count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
    
  end
end